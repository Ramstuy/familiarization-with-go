module redisred

go 1.14

require (
	github.com/go-chi/chi/v5 v5.0.12
	github.com/go-redis/redis/v8 v8.11.5
	github.com/google/go-cmp v0.5.9 // indirect
	github.com/google/uuid v1.6.0
	golang.org/x/net v0.10.0 // indirect
	golang.org/x/sys v0.14.0 // indirect
)
