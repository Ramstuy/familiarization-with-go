package main

import (
	"context"
	"fmt"
	"redisred/application"
)

func main() {
	app := application.New(application.LoadConfig())

	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()

	err := app.Start(ctx)
	if err != nil {
		fmt.Println("failed to start app:", err)
	}
}
