module pg

go 1.14

require (
	github.com/gofiber/fiber v1.14.6
	github.com/jinzhu/now v1.1.5 // indirect
	github.com/joho/godotenv v1.5.1
	github.com/kr/pretty v0.3.0 // indirect
	github.com/stretchr/testify v1.8.1 // indirect; indirectect
	gopkg.in/check.v1 v1.0.0-20201130134442-10cb98267c6c // indirect
	gorm.io/driver/postgres v0.2.4
	gorm.io/gorm v1.21.12
)
