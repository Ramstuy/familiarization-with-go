package detset

import (
	"fmt"
	"os"
	"runtime"
	"testing"
)

func TestHelloWorld(t *testing.T) {
	result := HelloWorld("Mark")

	if result != "Halo Mark" {
		// panic("Somehing went wrong")
		// t.Fail()
		t.Error("The returned value not equals to Halo Mark")
	}

	fmt.Println("Test 1 Executed")
}
func TestHelloWorld2(t *testing.T) {
	result := HelloWorld("Zukerbek")

	if result != "Halo Zukerbek" {
		// panic("Somehing went wrong")
		// t.FailNow()
		t.Fatal("The returned value not equals to Halo Zukerbek")
	}

	fmt.Println("Test 2 Executed")
}
func TestHelloWorld3(t *testing.T) {
	result := HelloWorld("Bruno")

	if result != "Halo Bruno" {
		// panic("Somehing went wrong")
		// t.FailNow()
		t.Fatal("The returned value not equals to Halo Bruno")
	}

	fmt.Println("Test 3 Executed")
}

func TestSkip(t *testing.T) {
	if runtime.GOOS == "windows" {
		t.Skip("Can not run on Windows")
	}

	fmt.Println("Actually your OS is", runtime.GOOS)

}

func TestMain(m *testing.M) {
	fmt.Println("Executing the test...")

	exitCode := m.Run()

	fmt.Println("Test executed.")

	os.Exit(exitCode)
}

func TestSubTest(t *testing.T) {
	t.Run("KDB", func(t *testing.T) {
		result := HelloWorld("KDB")

		if result != "Halo KDB" {
			t.Error("The returned value not equals to Halo KDB")
		}

		fmt.Println("Test KDB Executed")
	})
	t.Run("Messi", func(t *testing.T) {
		result := HelloWorld("Messi")

		if result != "Halo Messi" {
			t.Error("The returned value not equals to Halo Messi")
		}

		fmt.Println("Test Messi Executed")
	})
	t.Run("MVP", func(t *testing.T) {
		result := HelloWorld("MVP")

		if result != "Halo MVP" {
			t.Error("The returned value not equals to Halo MVP")
		}

		fmt.Println("Test MVP Executed")
	})
}

func BenchmarkHelloWorld(b *testing.B) {
	for i := 0; i < b.N; i++ {
		HelloWorld("Alecks")
	}
}

func BenchmarkHelloWorld2(b *testing.B) {
	for i := 0; i < b.N; i++ {
		HelloWorld("Xavier")
	}
}

func BenchmarkSub(b *testing.B) {
	b.Run("Wolverine", func(b *testing.B) {
		for i := 0; i < b.N; i++ {
			HelloWorld("Wolverine")
		}
	})
	b.Run("Dadpool", func(b *testing.B) {
		for i := 0; i < b.N; i++ {
			HelloWorld("Dadpool")
		}
	})
}

func TestHelloWorldTable(t *testing.T) {
	tests := []struct {
		name     string
		request  string
		expected string
	}{
		{
			name:     "HelloWorld(Kevin)",
			request:  "Kevin",
			expected: "Halo Kevin",
		}, {
			name:     "HelloWorld(De)",
			request:  "De",
			expected: "Halo De",
		}, {
			name:     "HelloWorld(Bruyne)",
			request:  "Bruyne",
			expected: "Halo Bruyne",
		},
	}

	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			result := HelloWorld(test.request)

			if result != test.expected {
				t.Error("The returned value not equals to", test.expected)
			}

			fmt.Println("Test Executed")
		})
	}

}
func BenchmarkHelloWorldTable(b *testing.B) {
	benchmarks := []struct {
		name    string
		request string
	}{
		{
			name:    "HelloWorld(Kevin)",
			request: "Kevin",
		}, {
			name:    "HelloWorld(De)",
			request: "De",
		}, {
			name:    "HelloWorld(Bruyne)",
			request: "Bruyne",
		},
	}

	for _, benchmark := range benchmarks {
		b.Run(benchmark.name, func(b *testing.B) {
			for i := 0; i < b.N; i++ {
				HelloWorld(benchmark.name)
			}
		})
	}

}
