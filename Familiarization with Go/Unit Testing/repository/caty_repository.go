package repository

import "detset/nity"

type CategoryRepository interface {
	FindById(id string) *nity.Category
}
