package dvred

import (
	"fmt"
	"testing"
	"time"
)

func TestCreateChannel(t *testing.T) {
	channel := make(chan string)

	go func() {
		time.Sleep(2 * time.Second)
		channel <- "Lewish"
		fmt.Println("Data send to channel")
	}()
	data := <-channel
	fmt.Println(data)
	time.Sleep(3 * time.Second)
}
