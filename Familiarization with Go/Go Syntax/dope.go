package main

import (
	"fmt"
	// _ "holloway/deep"
)

// type Customer struct {
// 	Name, Address string
// 	Age           int
// }

// type hasName interface {
// 	getName() string
// }

// type Person struct {
// 	Name string
// }

// type Animal struct {
// 	Name string
// }

// func womp() interface{} {
// 	return 1
// }

// type Address struct {
// 	City, Province, Country string
// }

// type Man struct {
// 	Name string
// }

// type error interface {
// 	Error() string
// }

type validationError struct {
	Message string
}

type notFoundError struct {
	Message string
}

func main() {
	//// Contoh print (tanda ',' setelah " " sebagai penghubung jika tipe data berbeda,
	//// untuk menghubugnkan string menggunakan ""+"".
	//// bisa juga digunakan untuk print 2 var dalam satu fmt.print() seperti fmt.Println(firstName + lastName)
	//// namun outputnya tidak akan menerapkan spasi diantara kedua variabel tersebut
	// fmt.Println("One = ", 1)
	// fmt.Println("One = " + "satu")
	// fmt.Println("One Point Six = ", 1.6)
	// fmt.Println("Teru = ", true)

	//// Contoh jika ingin mengambil karakter di index tertentu dalam string
	// fmt.Println("Aselole"[1])

	//// Contoh jika ingin menghitung panjang karakter
	// fmt.Println(len("Aselole"))

	//// Mendeklarasi variabel sebagai string
	// var name string

	//// Tanda := digunakan agar kita tidak perlu menginisialisasikan data pada var tersebut
	// name := "Elfonso"
	// fmt.Println(name)

	//// Misal dalam kasus ini saya mengubah nilai dari variabel nama diatas menjadi Alonso
	// name = "Alonso"
	// fmt.Println(name)

	//// Berikut contoh jika ingin mendefinisikan tipe banyak variabel sekaligus
	// var (
	// 	firstName  = "Alfonso"
	// 	middleName = "Daved"
	// 	lastName   = false
	// )

	// fmt.Println(firstName)
	// fmt.Println(middleName)
	// fmt.Println(lastName)

	//// contoh variabel konstan yang nilainya tidak dapat diubah, dan saat membuat variabel, harus dibuat dalam bentuk const ()
	// const (
	// 	firstName = "Kevin"
	// 	lastName  = "Bruyna"
	// )

	// fmt.Println(firstName + lastName)

	//// Mengubah tipe variable, khususnya size variabel yang cocok dengan nilai
	//// dalam kasus ini misal int32 bisa menganmpung var nilai32 tapi int16 tidak bisa menampungnya
	//// jadi dia kembali ke nilai terendah yang bisa ditampung tipe int16 ialah -32768 dan nilai tertingginya 32767
	// var (
	// 	nilai32 int32 = 32768
	// 	nilai64 int64 = int64(nilai32)
	// 	nilai16 int16 = int16(nilai64)
	// )

	// fmt.Println(nilai32)
	// fmt.Println(nilai64)
	// fmt.Println(nilai16)

	//// Contoh jika ingin mengambil satu karakter saja dalam nilai string suatu variabel
	//// jika sebelumnya jika kita hanya mengambil nilai nya menggunakan index karakter tanpa mendefinisikan tipe variabel string
	//// maka nilai dari variabel tersebut akan menjadi biner sesuai dengan karakter yang diambil
	// var (
	// 	name    = "Bruno"
	// 	r       = name[1]
	// 	b       = name[0]
	// 	u       = name[2]
	// 	rString = string(r)
	// 	bString = string(b)
	// 	uString = string(u)
	// )

	// fmt.Println(bString, rString, uString)

	//// Contoh membuat alias pada tipe variabel
	//// dalam kasus ini membuat tipe variabel string menjadi 'texted'. jadi, string=texted
	// type texted string

	// var (
	// 	postal texted = "2132131"
	// )

	// fmt.Println(postal)
	// fmt.Println(texted("35431423"))

	//// Contoh penggunaan operasi matematika
	// var x = 10
	// var y = 6
	// var sum = x + y
	// var subtract = x - y
	// var multiply = x * y
	// var divide = x / y
	// var mod = x % y

	// fmt.Println("The sum of x and y is", sum)
	// fmt.Println("The difference between x and y is", subtract)
	// fmt.Println("The product of x and y is", multiply)
	// fmt.Println("The quotient of x divided by y is", divide)
	// fmt.Println("The remainder of x divided by y is", mod)

	//// Contoh penggunaan augmented assignment
	//// maksud dari i += 23 itu sama dengan i = i + 23
	// var i = 23
	// i += 23
	// fmt.Println(i)

	// i += 3
	// fmt.Println(i)

	//// Contoh penggunaan unary operator
	// var j = 6
	// j++ // j = j + 1
	// fmt.Println(j)
	// j++ // j = j + 1
	// fmt.Println(j)
	// j-- // j = j - 1
	// fmt.Println(j)

	//// Contoh operasi perbandingan
	// var (
	// 	name1 = "Log"
	// 	name2 = "Log"
	// 	num1  = 10
	// 	num2  = 6

	// 	nameresult bool = name1 != name2
	// 	numresult  bool = num1 != num2
	// )

	// fmt.Println(nameresult)
	// fmt.Println(numresult)

	//// Contoh perbandingan bool
	// var (
	// 	nilaiAkhir = 90
	// 	absensi    = 80

	// 	lulusNilaiAkhir bool = nilaiAkhir >= 80
	// 	lulusAbsensi    bool = absensi >= 80

	// 	lulus bool = lulusNilaiAkhir && lulusAbsensi
	// )

	// fmt.Println(lulus)

	//// Contoh penggunaan array
	//// Menentukan jumlah banyak data yang akan diinput sebelum mentukan tipe data dari suatu variabel
	// var names [3]string

	// names[0] = "Go"
	// names[1] = "Run"
	// names[2] = "File"

	// fmt.Println(names)
	// fmt.Println(names[1])
	// fmt.Println(names[0])
	// fmt.Println(names[2])

	//// jika kita tidak mengisi data sebanyak storage yang kita tentukan sebelumnya
	//// maka akan ada nilai default seperti string kosong untuk string dan 0 untuk integer
	// var values = [3]int{
	// 	10,
	// 	06,
	// }

	// fmt.Println(values)

	//// Jika tidak ingin menentukan banyak data pada array
	//// cukup berikan '...' sebelum tipe data var
	// var values = [...]int{
	// 	10,
	// 	06,
	// 	2003,
	// }

	// fmt.Println("Datas inside the var: ", values)
	// fmt.Println("Total datas inserted: ", len(values))
	// values[0] = 100
	// fmt.Println("Modified datas: ", values)

	//// dua tipe pemanggilan slice (sama saja)
	// var chars = [...]string{
	// 	"Black",
	// 	"Knight",
	// 	"Blade",
	// 	"Lockjaw",
	// 	"Sif",
	// }

	// chars := [...]string{
	// 	"Black",
	// 	"Knight",
	// 	"Blade",
	// 	"Lockjaw",
	// 	"Sif",
	// }

	//// Dimulai dari index 1 dengan batas < index ke 4
	//// Misal 1:4 maka hanya akan menampilkan data pada index [1,2,3]
	// slice1 := chars[1:4]

	//// Dimulai dari index 0 dengan batas < index ke 2
	//// Misal :2 maka hanya akan menampilkan data pada index [0,1]
	// slice2 := chars[:2]

	//// Dimulai dari index ke 2 dan seterusnya sampai keseluruhan jumlah data
	//// Misal 2: maka akan menampilkan data pada index [2,...]
	// slice3 := chars[2:]

	//// Contoh format lain menggunakan slice
	//// untuk [:] itu menampilkan keseluruhan data pada array
	// var slice4 []string = chars[:]

	// fmt.Println(len(slice1))
	// fmt.Println(slice2)
	// fmt.Println(slice3)
	// fmt.Println(slice4)

	// days := [...]string{
	// 	"Senin",
	// 	"Selasa",
	// 	"Rabu",
	// 	"Kamis",
	// 	"Jum'at",
	// 	"Sabtu",
	// 	"Minggu",
	// }

	// daySlice1 := days[5:]
	// daySlice1[0] = "Sabtu Baru"
	// daySlice1[1] = "Minggu Baru"
	// fmt.Println(days)
	// fmt.Println(daySlice1)

	//// Append menambahkan data baru pada slice
	// daysSlice2 := append(daySlice1, "Libur Baru")
	// daysSlice2[1] = "Ups"
	// fmt.Println(daysSlice2)
	// fmt.Println(days)

	//// Contoh pembuatan Slice dari awal
	//// 2 contoh pemanggilan variabel (line 248 dan 250)
	// newSlice := make([]string, 2, 5)

	// var newSlice []string = make([]string, 2, 5)
	// newSlice[0] = "Eko"
	// newSlice[1] = "Oke"

	// fmt.Println(newSlice)
	// fmt.Println(len(newSlice))
	// fmt.Println(cap(newSlice))

	// newSlice2 := append(newSlice, "Wok")
	// fmt.Println(newSlice2)
	// fmt.Println(len(newSlice2))
	// fmt.Println(cap(newSlice2))

	// newSlice2[0] = "Doritos"
	// fmt.Println(newSlice2)
	// fmt.Println(newSlice)

	//// Contoh Copy slice
	// fromSlice := days[:]
	// toSlice := make([]string, len(fromSlice), cap(fromSlice))

	// copy(toSlice, fromSlice)

	// fmt.Println(fromSlice)
	// fmt.Println(toSlice)

	//// Perbedaan array dan slice
	// iniArray := [...]int{1, 2, 3, 4, 5}
	// iniSlice := []int{1, 2, 3, 4, 5}

	// fmt.Println(iniArray)
	// fmt.Println(iniSlice)

	//// beberapa tipe pemanggilan sintaks map
	// var person map[string]string = map[string]string{}
	// person["name"] = "Rams"
	// person["address"] = "Tuy"

	// person := map[string]string{
	// 	"name":      "Rams",
	// 	"address":   "Tuy",
	// 	"acumalaka": "aselole",
	// }

	// fmt.Println(person["name"])
	// fmt.Println(person["address"])
	// fmt.Println(person["acumalaka"])
	// fmt.Println(person)

	// book := make(map[string]string)
	// book["title"] = "Buku Go-Lang"
	// book["author"] = "Dattebayo"
	// book["wrong"] = "Salah"

	// fmt.Println(book)

	// delete(book, "wrong")

	// fmt.Println(book)

	////if expression
	// name := "lemon"

	// if name == "lemon" {
	// 	fmt.Println("Woi Lemon")
	// } else if name == "lemn" {
	// 	fmt.Println("Woi Lemn")
	// } else {
	// 	fmt.Println("Halo")
	// }

	// if length := len(name); length >= 5 {
	// 	fmt.Println("Panjang juga nama")
	// } else {
	// 	fmt.Println("Di bawah 5")
	// }

	//// Switch case
	// name := "Jok"

	// switch name {
	// case "Aselole":
	// 	fmt.Println("Hello Aselole")
	// case "Jok":
	// 	fmt.Println("Hello Jok")
	// default:
	// 	fmt.Println("Aloha amigo")
	// }

	// switch length := len(name); length >= 5 {
	// case true:
	// 	fmt.Println("Panjang juga nama")
	// case false:
	// 	fmt.Println("Di bawah 5")
	// }

	// length := len(name)
	// switch {
	// case length > 10:
	// 	fmt.Println("Nama Terlalu Panjang")
	// case length > 5:
	// 	fmt.Println("Nama Lumayan Panjang")
	// default:
	// 	fmt.Println("Nama Sudah Benar")
	// }

	//// For
	// for counter := 1; counter <= 10; counter++ {
	// 	fmt.Println("Perulangan ke ", counter)
	// }

	// names := []string{"Eko", "Kurniawan", "Khannedy"}
	// for i := 0; i < len(names); i++ {
	// 	fmt.Println(names[i])
	// }

	// for index, name := range names {
	// 	fmt.Println("index", index, "=", name)
	// }
	// for _, name := range names {
	// 	fmt.Println(name)
	// }

	// for i := 0; i < 10; i++ {
	// 	if i == 5 {
	// 		break
	// 	}
	// 	fmt.Println("Perulangan ke", i)
	// }

	// for i := 0; i < 10; i++ {
	// 	if i%2 == 0 {
	// 		continue
	// 	}
	// 	fmt.Println("Perulangan ke", i)
	// }

	// sayHelloTo("Kisanak", "Kisame")
	// sayHelloTo("Ro", "Or")

	// result := getHello("Stepen")
	// fmt.Println(result)

	// fmt.Println(getHello("Dope"))

	// firstName, lastName := getFullName()
	// fmt.Println(firstName, lastName)

	// _, lastName := getFullName()
	// fmt.Println(lastName)

	// a, b, c := getCompleteName()
	// fmt.Println(a, b, c)

	// total := sumAll(10, 10, 10, 10, 10)
	// fmt.Println(total)

	// numbers := []int{10, 10, 10, 10}
	// fmt.Println(sumAll(numbers...))

	// goodbye := getGoodBye
	// fmt.Println(goodbye("Dapid"))

	// sayHelloWithFilter("Lopez", spamFilter)

	// filter := spamFilter
	// sayHelloWithFilter("Bebek", filter)

	// blacklist := func(name string) bool {
	// 	return name == "dog"
	// }
	// registerUser("Dough", blacklist)
	// registerUser("dog", func(name string) bool {
	// 	return name == "dog"
	// })

	// fmt.Println(factorialLoop(10))

	// fmt.Println(factorialRecursive(10))

	// counter := 0

	// increment := func() {
	// 	fmt.Println("Melakukan increment...")
	// 	counter++
	// }

	// increment()
	// fmt.Println("Fungsi dipanggi sebanyak", counter, "kali")

	// runApplication()

	// runApp(true)

	// fmt.Println("App still working")

	// var cust Customer
	// cust.Name = "Doc"
	// cust.Address = "Jl. Kaki"
	// cust.Age = 20

	// fmt.Println(cust)
	// fmt.Println(cust.Name)

	// player := Customer{
	// 	Name:    "Spider-Man",
	// 	Age:     19,
	// 	Address: "Manhattan",
	// }

	// fmt.Println(player)
	// fmt.Println(player.Age)

	// buyer := Customer{"Strange", "New York", 31}
	// fmt.Println(buyer)
	// fmt.Println(buyer.Address)

	// vinn := Customer{Name: "Vinn"}

	// vinn.sayHello("Vinci")
	// player.sayHello("Sandman")

	// person := Person{Name: "Leo"}
	// sayHello(person)

	// animale := Animal{Name: "Tigris"}
	// sayHello(animale)

	// var pmow interface{} = womp()
	// fmt.Println(pmow)

	// datas := newMap("Qiq")

	// if datas == nil {
	// 	fmt.Println("Data not found")
	// } else {
	// 	fmt.Println(datas["name"])
	// }

	// var result interface{} = random()
	// var resultString string = result.(string)
	// fmt.Println(resultString)

	// result := random()
	// switch value := result.(type) {
	// case string:
	// 	fmt.Println("This value type is String:", value)
	// case int:
	// 	fmt.Println("This value type is Int:", value)
	// default:
	// 	fmt.Println("Type Unknown:", value)
	// }

	// var alamat1 Address = Address{"Makassar", "Sulsel", "Indonesia"}
	// var alamat2 *Address = &alamat1

	// alamat2.City = "Maros"
	// fmt.Println(alamat1)
	// fmt.Println(alamat2)

	// var alamat1 Address = Address{"Makassar", "Sulsel", "Indonesia"}
	// var alamat2 *Address = &alamat1

	// alamat2.City = "Maros"
	// fmt.Println(alamat1)
	// fmt.Println(alamat2)

	// // alamat2 = &Address{"Melbourne", "Australia", "Australia"}

	// *alamat2 = Address{"Melbourne", "Australia", "Australia"}
	// fmt.Println(alamat1)
	// fmt.Println(alamat2)

	// var alamat1 *Address = new(Address)
	// var alamat2 *Address = alamat1

	// alamat2.Country = "New Zealand"

	// fmt.Println(alamat1)
	// fmt.Println(alamat2)

	// ddres := Address{}
	// changeAddressTo(&ddres)

	// fmt.Println(ddres)

	// vndv := Man{"Davinci"}
	// vndv.Married()

	// fmt.Println(vndv.Name)

	// hasilImport := hood.SayHello("Vidi")
	// fmt.Println(hasilImport)

	// fmt.Println(hood.App)

	// fmt.Println(database.GetDatabase())

	// resultd, ero := divide(90, 0)
	// if ero == nil {
	// 	fmt.Println("Result", resultd)
	// } else {
	// 	fmt.Println("Something went wrong", ero.Error())
	// }

	rorr := SaveData("Opet", nil)
	if rorr != nil {
		// if validationRorr, wok := rorr.(*validationError); wok {
		// 	fmt.Println("id not valid:", validationRorr.Message)
		// } else if notFoundRorr, wok := rorr.(*notFoundError); wok {
		// 	fmt.Println("data error:", notFoundRorr.Message)

		// } else {
		// 	fmt.Println("unknown error:", rorr.Error())
		// }

		switch finalError := rorr.(type) {
		case *validationError:
			fmt.Println("id not valid:", finalError.Error())
		case *notFoundError:
			fmt.Println("data error:", finalError.Error())
		default:
			fmt.Println("unknown error:", finalError.Error())
		}
	} else {
		fmt.Println("Success")
	}
}

// func sayHelloTo(firstName string, lastName string) {
// 	fmt.Println("Hello", firstName, lastName)

// func getHello(name string) string {
// 	return "Hello " + name
// }

// func getFullName() (string, string) {
// 	return "Kevin", "Bruyne"
// }

// func getCompleteName() (firstName, middleName, lastName string) {
// 	firstName = "Logitech"
// 	middleName = "Rexus"
// 	lastName = "Fantech"

// 	return firstName, middleName, lastName
// }

// func sumAll(numbers ...int) int {
// 	total := 0
// 	for _, number := range numbers {
// 		total += number
// 	}
// 	return total
// }

// func getGoodBye(name string) string {
// 	return "GoodBye " + name
// }

// type Filter func(string) string

// func sayHelloWithFilter(name string, filter Filter) {
// 	fmt.Println("Hello", filter(name))
// }

// func spamFilter(name string) string {
// 	if name == "Bebek" {
// 		return "..."
// 	} else {
// 		return name
// 	}
// }

// type Blacklist func(string) bool

// func registerUser(name string, blacklist Blacklist) {
// 	if blacklist(name) {
// 		fmt.Println("You Are Blocked", name)
// 	} else {
// 		fmt.Println("Welcome", name)
// 	}
// }

// func factorialLoop(value int) int {
// 	result := 1

// 	for i := value; i > 0; i-- {
// 		result *= i
// 	}

// 	return result
// }

// func factorialRecursive(value int) int {
// 	if value == 1 {
// 		return 1
// 	} else {
// 		return value * factorialRecursive((value - 1))
// 	}
// }

// func logging() {
// 	fmt.Println("Selesai Run App")
// }

// func runApplication() {
// 	defer logging()
// 	fmt.Println("Run Application")
// }

// func endApp() {
// 	fmt.Println("End App")
// 	message := recover()
// 	fmt.Println("Something went wrong,", message)
// }

// func runApp(error bool) {
// 	defer endApp()
// 	if error {
// 		panic("Try Again")
// 	}
// }

// func (customer Customer) sayHello(name string) {
// 	fmt.Println("Sup", name+"!", "I go with", customer.Name)
// }

// func sayHello(isi hasName) {
// 	fmt.Println("Hello", isi)
// }

// func (person Person) getName() string {
// 	return person.Name
// }

// func (animal Animal) getName() string {
// 	return animal.Name
// }

// func newMap(name string) map[string]string {
// 	if name == "" {
// 		return nil
// 	} else {
// 		return map[string]string{
// 			"name": name,
// 		}
// 	}
// }

// func random() interface{} {
// 	return "true"
// }

// func changeAddressTo(addrs *Address) {
// 	addrs.Country = "Saranjana"
// }

// func (man *Man) Married() {
// 	man.Name = "Mr. " + man.Name
// }

// func divide(value, divider int) (int, error) {
// 	if divider == 0 {
// 		return 0, errors.New("divide by zero")
// 	} else {
// 		return value / divider, nil
// 	}
// }

func (v *validationError) Error() string {
	return v.Message
}
func (nf *notFoundError) Error() string {
	return nf.Message
}

func SaveData(id string, data interface{}) error {
	if id == "" {
		return &validationError{Message: "validation error"}
	}

	if id != "Opet" {
		return &notFoundError{Message: "data not found"}
	}

	return nil
}
